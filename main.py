import pandas as pd
import numpy as np
import re
import nltk
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.feature_extraction.text import CountVectorizer


def read_data(src):
    with open(src, 'r', encoding='utf-8') as f:
        text = f.read()
    return text


def prepare_text(text):
    # initilization
    wpt = nltk.WordPunctTokenizer()
    stop_words = nltk.corpus.stopwords.words('english')
    lemmatizer = WordNetLemmatizer()

    # pipeline
    # Filter only letter and space
    buff = ''.join(char for char in text if char.isalpha() or char.isspace())
    # lower register, delete spaces from right and left
    buff = buff.lower().strip()
    # tokenize text (roughly speaking, split text to array of words)
    tokens = wpt.tokenize(buff)
    # delete stop_words
    filtered_tokens = [token for token in tokens if token not in stop_words]
    # delete suffix, prefix etc at the word. Like infinitive word or standart state of word
    lemmatized_tokens = [lemmatizer.lemmatize(token) for token in filtered_tokens]
    return ' '.join(lemmatized_tokens)


def split_text_by_chapters(text):
    indexes_first_chapters = [match.start() for match in re.finditer(r'\bCHAPTER I\b', text)]
    if len(indexes_first_chapters) >= 2:
        buff_text = text[indexes_first_chapters[1]:]

    index_the_end = [match.start() for match in re.finditer(r'\bTHE END\b', buff_text)]
    buff_text = buff_text[:index_the_end[0]]
    return re.split(r'\bCHAPTER [IVXLCDM]+\b', buff_text)[1:]


def select_top_20_words_tf_idf(prepared_chapters):
    vectorizer = TfidfVectorizer(max_df=0.85, max_features=20)
    tf_idf_matrix = vectorizer.fit_transform(prepared_chapters)

    top_words = {}
    feature_names = vectorizer.get_feature_names_out()
    for i, row in enumerate(tf_idf_matrix):
        top_20_indices = row.toarray()[0].argsort()[-20:][::-1]
        top_20_features = [feature_names[index] for index in top_20_indices]
        top_words[f'Chapter {i+1}'] = top_20_features

    return top_words


def select_top_20_words_LDA(prepared_chapters):
    vectorizer = CountVectorizer(max_df=0.9, min_df=2)
    dtm = vectorizer.fit_transform(prepared_chapters)

    lda_model = LatentDirichletAllocation(n_components=len(prepared_chapters), random_state=322)
    lda_model.fit(dtm)

    top_words = {}
    feature_name = vectorizer.get_feature_names_out()
    for i, row in enumerate(lda_model.components_):
        top_20_indices = row.argsort()[-20:][::-1]
        top_20_features = [feature_name[index] for index in top_20_indices]
        top_words[f'Chapter {i+1}'] = top_20_features

    return top_words


if __name__ == '__main__':

    txt = read_data('./data/data.txt')
    chapters = split_text_by_chapters(txt)

    # nltk.download('stopwords')
    # nltk.download('wordnet')
    prepared_chapters = []
    for chapter in chapters:
        prepared_chapters.append(prepare_text(chapter))

    top_words_tf_idf = select_top_20_words_tf_idf(prepared_chapters)
    top_words_lda = select_top_20_words_LDA(prepared_chapters)

    with open('./output/result.txt', 'w') as f:
        f.write('=========== TF_IDF ===========\n')
        for chapter, top_words in top_words_tf_idf.items():
            f.write(f'{chapter} -> {top_words}\n')
        f.write('=========== LDA ===========\n')
        for chapter, top_words in top_words_lda.items():
            f.write(f'{chapter} -> {top_words}\n')
